# Phonebook

## запуск

### бэкэнд

```bash
git clone https://bitbucket.org/unti1x/phonebook-be.git
cd phonebook-be
composer install
```

структура базы в файле `migrations/0_init.sql`, конфиг для подключения в `config/db.php`

```bash
cd public && php -S localhost:8000
```

### фронтэнд

```bash
git clone https://bitbucket.org/unti1x/phonebook-fe.git
cd phonebook-be
yarn install
yarn start
```
