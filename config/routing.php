<?php

use Phonebook\Controller;
use Cheddar\Router\RouteMatcher;

return [
    RouteMatcher::class => [
        [
            'route' => '/^\/$/',
            'action' => [Controller\DefaultController::class, 'index']
        ],
        [
            'route' => '%^/api/contact$%',
            'action' => [Controller\ContactController::class, 'index'],
            'methods' => ['GET']
        ],
        [
            'route' => '%^/api/contact$%',
            'action' => [Controller\ContactController::class, 'create'],
            'methods' => ['POST']
        ],
        [
            'route' => '%^/api/contact/(?P<id>\d+)$%',
            'action' => [Controller\ContactController::class, 'show'],
            'methods' => ['GET']
        ],
        [
            'route' => '%^/api/contact/(?P<id>\d+)$%',
            'action' => [Controller\ContactController::class, 'update'],
            'methods' => ['PUT']
        ],
        [
            'route' => '%^/api/contact/(?P<id>\d+)$%',
            'action' => [Controller\ContactController::class, 'delete'],
            'methods' => ['DELETE']
        ],
        [
            'route' => '%^/api/contact/(?P<id>\d+)/phones$%',
            'action' => [Controller\PhoneController::class, 'create'],
            'methods' => ['POST']
        ],
        [
            'route' => '%^/api/contact/(?P<contactId>\d+)/phones/(?P<id>\d+)$%',
            'action' => [Controller\PhoneController::class, 'update'],
            'methods' => ['PUT']
        ],
        [
            'route' => '%^/api/contact/(?P<contactId>\d+)/phones/(?P<id>\d+)$%',
            'action' => [Controller\PhoneController::class, 'delete'],
            'methods' => ['DELETE']
        ],
    ]
];
