<?php

use Cheddar\DBAL\Schema;
use Phonebook\Entity\{Contact, Phone};

return [
    Schema::class => [
        Contact::class => [
            'table' => 'contact',
            'fields' => [
                'id' => ['type' => 'int'],
                'name' => ['type' => 'string']
            ],
            'relations' => [
                'phones' => [
                    'target' => Phone::class,
                    'type' => 'many',
                    'local' => 'id',
                    'foreign' => 'contact_id',
                    'inversed_by' => 'contact'
                ]
            ]
        ],
        Phone::class => [
            'table' => 'phone',
            'fields' => [
                'id' => ['type' => 'int'],
                'number' => ['type' => 'string'],
                'comment' => ['type' => 'string'],
                'contact_id' => ['type' => 'int']
            ]
        ]
    ]
];
