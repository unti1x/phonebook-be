<?php
use Phonebook\Kernel;
use Cheddar\Http\Request;

include_once '../vendor/autoload.php';

$kernel = new Kernel();
$kernel->boot();

$request = new Request($_GET, $_POST, $_SERVER);
$response = $kernel->handleRequest($request);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');

$kernel->send($response);
