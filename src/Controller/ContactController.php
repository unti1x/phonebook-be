<?php

namespace Phonebook\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Http\{Request, JsonResponse, Response};

use Phonebook\Repository\ContactRepository;
use Phonebook\Entity\Contact;
use Phonebook\Service\ContactSerializer;

class ContactController extends AbstractController
{
    /**
     *
     * @var ContactRepository
     */
    private $repo;
    
    /**
     *
     * @var ContactSerializer
     */
    private $serializer;
    
    public function __construct(ContactRepository $repo, ContactSerializer $serializer)
    {
        $this->repo = $repo;
        $this->serializer = $serializer;
    }
    
    public function index(Request $request): JsonResponse
    {
        $query = $request->getQuery()->get('q');
        
        $contacts = $this->repo->findAll($query);
        
        $data = array_map(function(Contact $contact)  {
            return $this->serializer->serialize($contact);
        }, $contacts);
        
        return $this->json(array_values($data));
    }
    
    public function show($id): JsonResponse
    {
        $contact = $this->repo->findById($id);
        if($contact) {
            $data = $this->serializer->serialize($contact);
            $response = $this->json($data);
        } else {
            $response = $this->json(
                ['error' => 'Not found'], 
                Response::HTTP_NOT_FOUND
            );
        }
        return $response;
    }
    
    public function create(Request $request): JsonResponse
    {
        $contact = $request->getRequest()->all();
        
        $id = $this->repo->insert($contact);
        return $this->json(['success' => true, 'id' => $id]);
    }
    
    public function update($id, Request $request): JsonResponse
    {
        $contact = $this->repo->find($id);
        if(!$contact) {
            return $this->json(['error' => 'Not found'], Response::HTTP_NOT_FOUND);
        }
        
        $name = $request->getRequest()->get('name');
        $this->repo->update($id, $name);
        return $this->json(['success' => true]);
    }
    
    public function delete($id): JsonResponse
    {
        /* 
         * паста в третий раз, потому что по-хорошему нужно кидать исключение,
         * но я так и не собрался написать обработчики со стороны фреймворка
         */
        $contact = $this->repo->find($id);
        if(!$contact) {
            return $this->json(['error' => 'Not found'], Response::HTTP_NOT_FOUND);
        }
        
        $this->repo->delete($id);
        return $this->json(['success' => true]);
    }
    
}
