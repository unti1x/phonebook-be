<?php

namespace Phonebook\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Http\Response;

class DefaultController extends AbstractController
{
    
    public function index()
    {
        return new Response('Mess with the best die like the rest');
    }
    
}
