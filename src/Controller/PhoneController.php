<?php

namespace Phonebook\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Http\{Request, JsonResponse};

use Phonebook\Repository\{ContactRepository, PhoneRepository};

class PhoneController extends AbstractController
{
    
    private $contactRepo;
    
    private $phoneRepo;
    
    public function __construct(
        ContactRepository $contactRepo, 
        PhoneRepository $phoneRepo
    )
    {
        $this->contactRepo = $contactRepo;
        $this->phoneRepo = $phoneRepo;
    }
    
    public function create($id, Request $request): JsonResponse
    {
        $contact = $this->contactRepo->find($id);
        if(!$contact) {
            return $this->json(['error' => 'Contact not found'], Response::HTTP_NOT_FOUND);
        }
        
        $data = $request->getRequest();
        
        $phoneId = $this->phoneRepo->insert(
            $id, 
            $data->get('number'),
            $data->get('comment')
        );
        
        return $this->json(['succes' => true, 'id' => $phoneId]);
    }
    
    public function update($id, Request $request): JsonResponse
    {
        $phone = $this->phoneRepo->find($id);
        if(!$phone) {
            return $this->json(['error' => 'Phone not found'], Response::HTTP_NOT_FOUND);
        }
        
        $this->phoneRepo->update($id, $request->getRequest()->get('comment'));
        return $this->json(['succes' => true]);
    }
    
    public function delete($id): JsonResponse
    {
        $phone = $this->phoneRepo->find($id);
        if(!$phone) {
            return $this->json(['error' => 'Phone not found'], Response::HTTP_NOT_FOUND);
        }
        
        $this->phoneRepo->delete($id);
        return $this->json(['succes' => true]);
    }
    
}
