<?php

namespace Phonebook\Entity;

class Contact
{
    /**
     *
     * @var int
     */
    private $id;
    
    /**
     *
     * @var string
     */
    private $name;
    
    /**
     *
     * @var Phone[]
     */
    private $phones = [];
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 
     * @return Phone[]
     */
    public function getPhones(): array
    {
        return $this->phones;
    }

    public function addPhones(Phone $phone): self
    {
        $this->phones[] = $phone;
        return $this;
    }
    
}
