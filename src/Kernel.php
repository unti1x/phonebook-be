<?php

namespace Phonebook;

use Cheddar\Kernel as BaseKernel;
use Cheddar\DependencyInjection\ConfigurationLoader;

class Kernel extends BaseKernel
{
    
    protected function loadParameters()
    {
        $parameters = ConfigurationLoader::load($this->baseDir . '/config/*.php');
        $this->parameters->setParameters($parameters);
    }

}
