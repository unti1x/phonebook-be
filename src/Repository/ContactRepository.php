<?php

namespace Phonebook\Repository;

use Cheddar\DBAL\{AbstractRepository, EntityManager, DatabaseAdapter};
use Phonebook\Entity\Contact;

class ContactRepository extends AbstractRepository
{
    
    public function __construct(EntityManager $em, DatabaseAdapter $adapter)
    {
        parent::__construct($em, $adapter);
        $this->setEntity(Contact::class);
    }
    
    public function findAll(?string $search = null): ?iterable
    {
        $query = $this->getQuery()->with('phones');
        
        if($search) {
            $query->filter('`contact`.`name` LIKE :name OR `phones`.`number` LIKE :phone');
            $query->setParameters([
                ':name' => "%$search%",
                ':phone' => "$search%"
            ]);
        }

        return $this->getResult($query);
    }
    
    public function findById($id): ?Contact
    {
        $query = $this->getQuery()
            ->with('phones')
            ->filter('`contact`.`id` = :id')
            ->setParameters([':id' => $id]);
        
        return $this->getSingleResult($query);
    }
    
    public function insert($contact)
    {
        $query = 'INSERT INTO `contact` (`name`) VALUES (:name)';
        $id = $this->executeSql($query, [':name' => $contact['name']]);
        
        $phoneQuery = 'INSERT INTO `phone` (`number`, `comment`, `contact_id`) '
            . 'VALUES (:number, :comment, :contact_id)';
        
        foreach($contact['phones'] as $phone) {
            $this->executeSql($phoneQuery, [
                ':number' => $phone['number'],
                ':comment' => $phone['comment'],
                ':contact_id' => $id
            ]);
        }
        
        return $id;
    }
    
    public function update($id, string $name): void
    {
        $query = 'UPDATE `contact` SET `name` = :name WHERE `id` = :id';
        $this->executeSql($query, [':id' => $id, ':name' => $name]);
    }
    
    
    public function delete($id): void
    {
        $phonesQuery = 'DELETE FROM `phone` WHERE `contact_id` = :id';
        $this->executeSql($phonesQuery, [':id' => $id]);
        
        $query = 'DELETE FROM `contact` WHERE `id` = :id';
        $this->executeSql($query, [':id' => $id]);
    }
    
}
