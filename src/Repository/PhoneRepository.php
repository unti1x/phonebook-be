<?php

namespace Phonebook\Repository;

use Phonebook\Entity\Phone;
use Cheddar\DBAL\{AbstractRepository, EntityManager, DatabaseAdapter};

class PhoneRepository extends AbstractRepository
{
    
    public function __construct(EntityManager $em, DatabaseAdapter $adapter)
    {
        parent::__construct($em, $adapter);
        $this->setEntity(Phone::class);
    }
    
    public function insert($contactId, string $number, string $comment)
    {
        $query = 'INSERT INTO `phone` (`contact_id`, `number`, `comment`) '
            . 'VALUES (:id, :number, :comment)';
        
        $id = $this->executeSql($query, [
            ':id' => $contactId, 
            ':number' => $number,
            ':comment' => $comment
        ]);
        
        return $id;
    }
    
    public function update($id, string $comment)
    {
        $query = 'UPDATE `phone` SET `comment` = :comment';
        $this->executeSql($query, [':comment' => $comment]);
    }
    
    public function delete($id)
    {
        $query = 'DELETE FROM `phone` WHERE `id` = :id';
        $this->executeSql($query, [':id' => $id]);
    }
    
}
