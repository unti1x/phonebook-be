<?php

namespace Phonebook\Service;

use Phonebook\Entity\{Contact, Phone};

class ContactSerializer
{
    
    private function serializePhone(Phone $phone)
    {
        return [
            'id' => $phone->getId(),
            'number' => $phone->getNumber(),
            'comment' => $phone->getComment()
        ];
    }
    
    public function serialize(Contact $contact): array
    {
        $phones = array_map([$this, 'serializePhone'], $contact->getPhones());
        return [
            'id' => $contact->getId(),
            'name' => $contact->getName(),
            'phones' => array_values($phones)
        ];
    }
    
}
